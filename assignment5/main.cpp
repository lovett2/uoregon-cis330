/* Kathryn Lovett
   CIS 330
   Winter 2016
   Assignment 5: Conway's Game of Life
*/

using namespace std;
#include <iostream>
#include "Game.hpp"

int main(){
	// Declaring variables height, width, and steps
	int height;
	int width;
	int steps;

	// Prompting user for input of size of grid and number of steps to take
	cout << "Please enter the size of the grid (int int): ";
	cin >> height >> width;

	cout << "Please enter the number of steps (int): ";
	cin >> steps;

	// Initializing game and printing out original board
	Game myGame(height, width);
	myGame.initialize();
	myGame.Print();
	cout << endl;

	// Applying rules for the number of steps specificed, printing board out
	// after each time
	for(int i = 1; i <= steps; i++){
		cout << "Step " << i << ":" << endl;
		myGame.transition();
		myGame.Print();
		cout << endl;
	}
	
	return 0;
}
/* Kathryn Lovett
   CIS 330
   Winter 2016
   Assignment 5: Conway's Game of Life
*/
   
#ifndef GAME_H_
#define GAME_H_

/* Class Game definition*/
class Game {
private:
	char **array;
	int height;
	int width;
	
public:
	Game(int, int);
	~Game();
	char** initialize();
	void transition();
	void Print();

	void checkRules(char, int, int);
	void farmerFunction(int, int);
};


#endif /* GAME_H_ */

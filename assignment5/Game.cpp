/* Kathryn Lovett
   CIS 330
   Winter 2016
   Assignment 5: Conway's Game of Life
*/

using namespace std;
#include <iostream>
#include <cstdlib>
#include <stdio.h>
#include "Game.hpp"

const char sheep = 'S';
const char farmer = 'F';
const char wolf = 'W';
const char empty = '.';

// Declaring constructor for Game Class
Game::Game(int h, int w){
	this->height = h;
	this->width = w;
	this->array = new char*[this->height];
	for(int i = 0; i < this->height; i++){
		this->array[i] = new char[this->width];
	}
}

// Declaring destructor for Game Class
Game::~Game(){
	for(int i = 0; i < this->height; i++){
		free(this->array[i]);
	}
	free(this->array);
}

// Initializes 2D array allocated in constructor with random
// input of either farmer, sheep, wolf, or empty
char** Game::initialize(){
	srand(time(NULL));
	int randNum;

	for(int i = 0; i < this->height; i++) {
		for(int j = 0; j < this->width; j++) {
			randNum = rand() % 4 + 1;
			switch(randNum){
				case 1: 
					this->array[i][j] = sheep;
					break;
				case 2:
					this->array[i][j] = wolf;
					break;
				case 3:
					this->array[i][j] = farmer;
					break;
				case 4:
					this->array[i][j] = empty;
					break;
			}
		}
	}
	return array;
}

// Prints out 2D array
void Game::Print(){
	for(int i = 0; i < this->height; i++){
		for(int j = 0; j < this->width; j++){
			cout << this->array[i][j];
		}
		cout << endl;
	}
}

// Goes through board, and depending on whether it's a sheep,
// wolf, farmer, or empty, it calls a function
void Game::transition(){
	for(int i = 0; i < this->height; i++) {
		for(int j = 0; j < this->width; j++) {
			switch(this->array[i][j]){
				case sheep:
					checkRules(sheep, i, j);
					break;
				case empty:
					checkRules(empty, i, j);
					break;
				case farmer:
					farmerFunction(i, j);
					break;
				case wolf:
					checkRules(wolf, i, j);
					break;
				
			}
		}
	}
}

// Based upon the surrounding cells and the center cell, certain
// rules are applied. This function checks the surrounding cells, 
// incrememnting what it finds, and then determines what to do 
// based on that
void Game::checkRules(char c, int i, int j){
	int sheepCount = 0;
	int wolfCount = 0;
	int farmerCount = 0;
	int emptyCount = 0;

	for(int k = i - 1; k <= i + 1; k++){
		for(int l = j - 1; l <= j + 1; l++){
			if(k >= 0 && k < this->height && l >= 0 && l < this->width){
				switch(this->array[k][l]){
					case sheep:
						sheepCount++;
						break;
					case wolf:
						wolfCount++;
						break;
					case farmer:
						farmerCount++;
						break;
					case empty:
						emptyCount++;
						break;
				}
			}
		}
	}

	// If the central cell is a sheep, and the sheep count is 3 or the
	// wolf count is greater than 0, the sheep dies and the cell is empty
	if(c == sheep && (sheepCount > 3 || wolfCount > 0)){
		this->array[i][j] = empty;
	}
	// If the central cell is empty and there is two of something else surrounding 
	// it, then they reproduce and it is then populated with that element
	else if(c == empty){
		if(sheepCount == 2){
			this->array[i][j] = sheep;
		}
		else if(wolfCount == 2){
			this->array[i][j] = wolf;
		}
		else if(farmerCount == 2){
			this->array[i][j] = farmer;
		}
	}
	// If the central cell is a wolf and the farmer count is greater than 0, or
	// the farmer count is 0 and the sheep count is 0, the wolf dies and the
	// cell changes to empty
	else if(c == wolf){
		if(farmerCount > 0){
			this->array[i][j] = empty;
		}
		else if(farmerCount == 0 && sheepCount == 0){
			this->array[i][j] = empty;
		}
	}
}

// If the central cell is a farmer and there is at least
// one empty cell nearby, it is randomly moved to an empty cell
void Game::farmerFunction(int i, int j){
	int emptyCount = 0;
	int xEmpty[8];
	int yEmpty[8];
	for(int k = i - 1; k <= i + 1; k++){
		for(int l = j - 1; l <= j + 1; l++){
			if(k >= 0 && k < this->height && l >= 0 && l < this->width){
				if(this->array[k][l] == empty){
					xEmpty[emptyCount] = k;
					yEmpty[emptyCount] = l;
					emptyCount++;
				}
			}
		}
	}
	srand(time(NULL));
	
	if(emptyCount > 0){
		int randNum = rand() % emptyCount;

		this->array[xEmpty[randNum]][yEmpty[randNum]] = farmer;
		this->array[i][j] = empty;
	}
}

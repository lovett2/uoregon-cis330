#ifndef POWER_H_
#define POWER_H_

/* Compute a^b (b is the exponent) */
double power(double a, double b);


#endif /* POWER_H_ */

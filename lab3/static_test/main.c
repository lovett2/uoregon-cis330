#include <stdio.h>
#include "power.h"

int main() {
	double one = 10.0;
	double two = 2.0;

	double raised = power(one, two);
	printf("%.02lf raised to %.02lf is %.02lf\n", one, two, raised);
	return 0;
}

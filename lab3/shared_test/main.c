#include <stdio.h>
#include "power.h"

int main() {
	double one = 6.0;
	double two = 3.0;

	double new = power(one, two);
	printf("%.02lf raised to %.02lf is %.02lf\n", one, two, new);
	return 0;
}

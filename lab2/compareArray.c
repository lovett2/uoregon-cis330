#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#define ROW 3
#define COL 4

bool arrayEqual(int a[ROW][COL], int b[ROW][COL], int m, int n)
{
    // Run through each element in both 2D arrays and return
    // false if there is a mismatch. If it finishes going through
    // all elements without a mismatch, return true.

    for(int i = 0; i < m; i++){
	for(int j = 0; j < n; j++){
             if(a[i][j] != b[i][j]){
		 return false;
             };
	};
    };
    return true; 

}

bool arrayEqual2(int** a, int** b, int m, int n)
{
    // Run through each element in both 2D arrays and return
    // false if there is a mismatch. If it finishes going through
    // all elements without a mismatch, return true.

    for(int i = 0; i < m; i++){
        for(int j = 0; j < n; j++){
             if(a[i][j] != b[i][j]){
                 return false;
             };
        };
    };
    return true;

}

int main(int argc, const char * argv[])
{

    int a[ROW][COL] = {
        {0, 1, 2, 3} ,
        {4, 5, 6, 7} ,
        {8, 9, 10, 11}
    };

    int b[ROW][COL] = {
        {0, 1, 2, 3} ,
        {4, 5, 6, 7} ,
        {8, 9, 10, 11}   
    };

    // Create two 2D arrays a2 and b2 with dynamic memory allocation,
    // then fill the arrays.
    
    int** a2 = (int **)malloc(2 * sizeof(int *));
    for(int i = 0; i < 2; ++i) {
	a2[i] = (int *)malloc(3 * sizeof(int));
    };
    
    int count = 0;
    for(int i = 0; i < 2; i++){
	for(int j = 0; j < 3; j++){
	     a2[i][j] = ++count;
	};
    };

    int** b2 = (int **)malloc(2 * sizeof(int *));
    for(int i = 0; i < 2; ++i) {
        b2[i] = (int *)malloc(3 * sizeof(int));
    };
    
    count = 1;
    for(int i = 0; i < 2; i++){
        for(int j = 0; j < 3; j++){
             b2[i][j] = ++count;
        };
    };

    // Call the method arrayEqual to check whether the 2D arrays
    // a and b are equal, and assign the result to the boolean areEqual.
    // Print the result.

    bool areEqual = arrayEqual(a, b, ROW, COL);

    if(areEqual){
	printf("The two arrays are equal!\n");
    }
    else {
	printf("The two arrays are unequal.\n");
    };
    
    // Call the method arrayEqual2 and assign the result to areEqual2.
    // Then print whether it returned true or false.

    bool areEqual2 = arrayEqual2(a2, b2, 2, 3);
    printf("%s\n", areEqual2 ? "true" : "false");

    free(a2);
    free(b2);

    return 0;
}

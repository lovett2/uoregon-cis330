/*
 * Kathryn Lovett
 * CIS 330, W16
 * Assignment4: C++ Practice
 */
#include "MorseEncoder.hpp"
#include "Arduino.h"
#include <cstring>

string LightEncoder::encode(string morseCode) {
  // take in string morseCode and run through each char in string
  // depending on if it is '.', '-', or ' ', perform a different
  // light pattern
  for(int i = 0; i < morseCode.size(); i++) {
    if(morseCode[i] != NULL) {
      switch(morseCode[i]) {
        case '.':
          digitalWrite(13, HIGH); 
          delay(1000);
          digitalWrite(13, LOW);
          delay(1000);
          break;
        case '-':
          digitalWrite(13, HIGH); 
          delay(3000);
          digitalWrite(13, LOW);
          delay(1000);
          break;
        case ' ':
          digitalWrite(13, LOW);
          delay(3000);
          break;
      }
    }

  }
  return "";
}


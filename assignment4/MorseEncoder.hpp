/*
 * Kathryn Lovett
 * CIS 330, W16
 * Assignment4: C++ Practice
 */
using namespace std;
#include <string>
#ifndef MORSEENCODER_H_
#define MORSEENCODER_H_


/* Abstract MorseEncoder class that has a method "encode" that takes
 *  as input a string and produces an array that represents the morse encoding
 *  in the traditional way using dots, dashes, and spaces.
 */
 class MorseEncoder {
    
  public:
//    MorseEncoder();
//    ~MorseEncoder();
    virtual string encode(string);
 };

/* Class that inherits from the MorseEncoder class. Takes string of 
 * morse code and translates that into blinking lights.
 */
class LightEncoder : public MorseEncoder {
  public:
    string encode(string);   
};


#endif /* MORSEENCODER_H_ */


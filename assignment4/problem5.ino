/*
 * Kathryn Lovett
 * CIS 330, W16
 * Assignment4: C++ Practice
 */
#include "MorseEncoder.hpp"

void setup() {
  // initialize digital pin 13 as an output.
  Serial.begin(9600);
  pinMode(13, OUTPUT);
}

void loop() {
  // declare char array of 10 words to transmit in morse code
  char arr[10][10] = {"blue", "eel", "breakfast", "coffee", "love",
    "leaf", "bluff", "free", "charm", "ale"};

  // generate random int to grab random word from array
  int r = rand() % 10;

  // create instances of MorseEncoder and LightEncoder class
  MorseEncoder *m = new MorseEncoder();
  LightEncoder *l = new LightEncoder();
  
  // create for loop that will repeat word three times
  for(int i = 0; i < 3; i++) {
    // convert word from 2d array arr to string
    std::string alphaString = arr[r];

    // encode alphaString to morse code and assigning to string morse
    std::string morse = m->encode(alphaString);

    // call lightPattern and interpret string morse into light
    l->encode(morse);
  }
  // print out the word after it has been shown three times
  Serial.println(arr[r]);

  // put a five second gap in between each new word
  digitalWrite(13, LOW);
  delay(5000);

  // free up the space allocated to m and l
  delete m;
  delete l;
}


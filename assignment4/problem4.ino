/*
 * Kathryn Lovett
 * CIS 330, W16
 * Assignment4: Using sensors
 */

const int microphone = 2;
const int ledOnPin = 13;
const int ledOffPin = 12;


void setup() {
  //start serial connection
  Serial.begin(9600);
  //configure pin2 as an input and enable the internal pull-up resistor
  pinMode(microphone, INPUT_PULLUP);
  // setup pin12 and pin13 as output
  pinMode(ledOnPin, OUTPUT);
  pinMode(ledOffPin, OUTPUT);

}

void loop() {
  //read the sensor value into a variable
  int sensorVal = digitalRead(microphone);
  //print out the value of the sensor
  Serial.println(sensorVal);

  //turn the red LED on if there is sound
  //turn the blue LED on if there isn't sound
  if (sensorVal == HIGH) {
    digitalWrite(ledOnPin, LOW);
    digitalWrite(ledOffPin, HIGH);
  } else {
    digitalWrite(ledOffPin, LOW);
    digitalWrite(ledOnPin, HIGH);
  }
  delay(30);
}


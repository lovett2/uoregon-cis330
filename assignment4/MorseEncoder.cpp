/*
 * Kathryn Lovett
 * CIS 330, W16
 * Assignment4: C++ Practice
 */
#include "MorseEncoder.hpp"
#include <cstring>

string MorseEncoder::encode(string myString) {
  // encodes string, changing letters to their equivalent in morse code
  // declaring string arr to hold newly encoded string
  string arr = "";

  // loop through char in string and append the equivalent in morse
  // code to the string arr
  for(int i = 0; i < myString.size(); i++) {
    
    switch(myString[i]) {
      case 'a':
        arr += ".-";
        break;
      case 'b':
        arr += "-...";
        break;
      case 'c':
        arr += "-.-.";
        break;
      case 'd':
        arr += "-..";
        break;
      case 'e':
        arr += '.';
        break;
      case 'f':
        arr += "..-.";
        break;
      case 'g':
        arr += "--.";
        break;
      case 'h':
        arr += "....";
        break;
      case 'i':
        arr += "..";
        break;
      case 'j':
        arr += ".---";
        break;
      case 'k':
        arr += "-.-";
        break;
      case 'l':
        arr += ".-..";
        break;
      case 'm':
        arr += "--";
        break;
      case 'n':
        arr += "-.";
        break;
      case 'o':
        arr += "---";
        break;
      case 'p':
        arr += ".--.";
        break;
      case 'q':
        arr += "--.-";
        break;
      case 'r':
        arr += ".-.";
        break;
      case 's':
        arr += "...";
        break;
      case 't':
        arr += '-';
        break;
      case 'u':
        arr += "..-";
        break;
      case 'v':
        arr += "...-";
        break;
      case 'w':
        arr += ".--";
        break;
      case 'x':
        arr += "-..-";
        break;
      case 'y':
        arr += "-.--";
        break;
      case 'z':
        arr += "--..";
        break;
    }
    // after each char append a space to indicate a new char
    arr += " ";
  }
  // after encoding the entire string, return the morse code version
  return arr;
}

/*
 * Kathryn Lovett
 * CIS 330, W16
 * Assignment4: Blink++
 */

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin 13 as an output.
  Serial.begin(9600);
  
  pinMode(13, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  // declare char array of 10 words to transmit in morse code
  char arr[10][10] = {"blue", "eel", "breakfast", "coffee", "love",
    "leaf", "bluff", "free", "charm", "ale"};

  // generate random int to grab random word from array
  int r = rand() % 10;

  // create for loop that will repeat three times
  for(int i = 0; i < 3; i++) {
    // create for loop that will run through each char in word
    for(int j = 0; j < sizeof(arr[r]); j++) {
      // if the char isn't null, perform a switch that calls a function
      // based upon what char it is
      if(arr[r][j] != NULL) {
        switch(arr[r][j]) {
          // each function makes the pin 13 light blink in a pattern
          // corresponding to morse code for that particular char
          case 'a':
            charA();
            break;
          case 'b':
            charB();
            break;
          case 'c':
            charC();
            break;
          case 'e':
            charE();
            break;
          case 'f':
            charF();
            break;
          case 'h':
            charH();
            break;
          case 'k':
            charK();
            break;
          case 'l':
            charL();
            break;
          case 'm':
            charM();
            break;
          case 'o':
            charO();
            break;
          case 'r':
            charR();
            break;
          case 's':
            charS();
            break;
          case 't':
            charT();
            break;
          case 'u':
            charU();
            break;
          case 'v':
            charV();
            break;
          }
          // after each char wait three seconds
          digitalWrite(13, LOW);
          delay(3000); 
      } // End if statement
    } // End inner for loop
    // after each word wait five seconds
    digitalWrite(13, LOW);
    delay(5000);
  } // End outer for loop
  // after each word has run three times print the word
  Serial.println(arr[r]);
  // wait two more seconds before proceeding to the next word
  digitalWrite(13, LOW);
  delay(2000);
}

void charA() {
  digitalWrite(13, HIGH); 
  delay(1000); 
  digitalWrite(13, LOW);
  delay(1000);
  digitalWrite(13, HIGH); 
  delay(3000); 
}

void charB() {
  digitalWrite(13, HIGH); 
  delay(3000); 
  digitalWrite(13, LOW);
  delay(1000);
  digitalWrite(13, HIGH); 
  delay(1000);
  digitalWrite(13, LOW);
  delay(1000);
  digitalWrite(13, HIGH); 
  delay(1000);
  digitalWrite(13, LOW);
  delay(1000); 
  digitalWrite(13, HIGH); 
  delay(1000);  
}

void charC() {
  digitalWrite(13, HIGH); 
  delay(3000); 
  digitalWrite(13, LOW);
  delay(1000);
  digitalWrite(13, HIGH); 
  delay(1000);
  digitalWrite(13, LOW);
  delay(1000);
  digitalWrite(13, HIGH); 
  delay(3000);
  digitalWrite(13, LOW);
  delay(1000); 
  digitalWrite(13, HIGH); 
  delay(1000);    
}

void charE() {
  digitalWrite(13, HIGH); 
  delay(1000);         
}

void charF() {
  digitalWrite(13, HIGH); 
  delay(1000); 
  digitalWrite(13, LOW);
  delay(1000);
  digitalWrite(13, HIGH); 
  delay(1000);
  digitalWrite(13, LOW);
  delay(1000);
  digitalWrite(13, HIGH); 
  delay(3000);
  digitalWrite(13, LOW);
  delay(1000); 
  digitalWrite(13, HIGH); 
  delay(1000);            
}

void charH() {
  digitalWrite(13, HIGH); 
  delay(1000); 
  digitalWrite(13, LOW);
  delay(1000);
  digitalWrite(13, HIGH); 
  delay(1000);
  digitalWrite(13, LOW);
  delay(1000);
  digitalWrite(13, HIGH); 
  delay(1000);
  digitalWrite(13, LOW);
  delay(1000); 
  digitalWrite(13, HIGH); 
  delay(1000);           
}


void charK() {
  digitalWrite(13, HIGH); 
  delay(3000); 
  digitalWrite(13, LOW);
  delay(1000);
  digitalWrite(13, HIGH); 
  delay(1000);
  digitalWrite(13, LOW);
  delay(1000);
  digitalWrite(13, HIGH); 
  delay(3000);            
}

void charL() {
  digitalWrite(13, HIGH); 
  delay(1000); 
  digitalWrite(13, LOW);
  delay(1000);
  digitalWrite(13, HIGH); 
  delay(3000);
  digitalWrite(13, LOW);
  delay(1000);
  digitalWrite(13, HIGH); 
  delay(1000);
  digitalWrite(13, LOW);
  delay(1000); 
  digitalWrite(13, HIGH); 
  delay(1000);            
}

void charM() {
  digitalWrite(13, HIGH); 
  delay(3000); 
  digitalWrite(13, LOW);
  delay(1000);
  digitalWrite(13, HIGH); 
  delay(3000);      
}

void charO() {
  digitalWrite(13, HIGH); 
  delay(3000); 
  digitalWrite(13, LOW);
  delay(1000);
  digitalWrite(13, HIGH); 
  delay(3000);
  digitalWrite(13, LOW);
  delay(1000);
  digitalWrite(13, HIGH); 
  delay(3000);        
}

void charR() {
  digitalWrite(13, HIGH); 
  delay(1000); 
  digitalWrite(13, LOW);
  delay(1000);
  digitalWrite(13, HIGH); 
  delay(3000);
  digitalWrite(13, LOW);
  delay(1000);
  digitalWrite(13, HIGH); 
  delay(1000);       
}

void charS() {
  digitalWrite(13, HIGH); 
  delay(1000); 
  digitalWrite(13, LOW);
  delay(1000);
  digitalWrite(13, HIGH); 
  delay(1000);
  digitalWrite(13, LOW);
  delay(1000);
  digitalWrite(13, HIGH); 
  delay(1000);           
}

void charT() {
  digitalWrite(13, HIGH); 
  delay(3000);         
}

void charU() {
  digitalWrite(13, HIGH); 
  delay(1000); 
  digitalWrite(13, LOW);
  delay(1000);
  digitalWrite(13, HIGH); 
  delay(1000);
  digitalWrite(13, LOW);
  delay(1000);
  digitalWrite(13, HIGH); 
  delay(3000);         
}

void charV() {
  digitalWrite(13, HIGH); 
  delay(1000); 
  digitalWrite(13, LOW);
  delay(1000);
  digitalWrite(13, HIGH); 
  delay(1000);
  digitalWrite(13, LOW);
  delay(1000);
  digitalWrite(13, HIGH); 
  delay(1000);
  digitalWrite(13, LOW);
  delay(1000);
  digitalWrite(13, HIGH); 
  delay(3000);            
}



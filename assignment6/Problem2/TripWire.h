/*
 * Kathryn Lovett
 * CIS 330, W16
 * Assignment6: Trip Wire
 */
using namespace std;
#include <string>
#ifndef TRIPWIRE_H_
#define TRIPWIRE_H_

class Alarm {
public:
  int alarmDuration = 1000;
  int builtinLight = 13;
  int buzzerPin = 5;
  
  Alarm();
  ~Alarm();
  void trigger();
  void Stop();
  void setDuration(int duration);
};

class BlinkAlarm: public Alarm {
public:
  BlinkAlarm();
  ~BlinkAlarm();
  void trigger();
  void Stop();
};
#endif /* TRIPWIRE_H_ */

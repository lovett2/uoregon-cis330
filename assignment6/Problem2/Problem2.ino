/*
 * Kathryn Lovett
 * CIS 330, W16
 * Assignment6: Trip Wire
 */
#include "TripWire.h"
 
int photoResistorPin = 0;  // Analog 0
int laserPtrPin = 3;
int lightVal = 0;
int threshold = 975;

void setup() {
  Alarm *a = new Alarm();
  pinMode(laserPtrPin, OUTPUT);  // laser pointer
  pinMode(a->builtinLight, OUTPUT); // built-in light
  Serial.begin(9600);
}

void loop() {
  digitalWrite(laserPtrPin, HIGH);
  lightVal = analogRead(photoResistorPin);
  Serial.println(lightVal);
  
  if (lightVal < threshold) { 
    BlinkAlarm *a = new BlinkAlarm();
    a->trigger();
    a->Stop();
  }
}


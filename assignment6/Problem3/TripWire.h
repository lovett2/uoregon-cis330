/*
 * Kathryn Lovett
 * CIS 330, W16
 * Assignment6: Trip Wire
 */
using namespace std;
#include <string>
#include <iostream>
//#include <MQTTClient.h>
#ifndef TRIPWIRE_H_
#define TRIPWIRE_H_

class Alarm {
public:
  int alarmDuration = 1000;
  int builtinLight = 13;
  int buzzerPin = 5;
  
  Alarm();
  ~Alarm();
  void trigger();
  void Stop();
  void setDuration(int duration);
};

class BlinkAlarm: public Alarm {
public:
  BlinkAlarm();
  ~BlinkAlarm();
  void trigger();
  void Stop();
};

class BuzzerAlarm: public Alarm {
public:
  BuzzerAlarm();
  ~BuzzerAlarm();
  void trigger();
  void Stop();
};

enum security_mode {OPEN = 0, SSL = 1, PSK = 2};

class MQTTClient {
public:
  MQTTClient();
  ~MQTTClient();
  void begin(char * broker, int port, security_mode mode,
                  char* certificate_file, char *psk_identity, char *psk);
  bool publish(char *topic, char *message);
  bool subscribe(char* topic, void (*callback)(char *topic, char* message));
  bool loop();
  bool available();
  void close();
private:
  void parseDataBuffer();
  FILE* spipe;
  char mqtt_broker[32];
  security_mode  mode;
  char topicString[64];
  char certificate_file[64];
  char psk_identity[32];
  char psk_password[32];
  int serverPort;
  char *topic;
  char *message;
  bool retain_flag;
  void (*callback_function)(char* topic, char* message);
  char dataBuffer[256];
};


class CloudAlarm: public Alarm {
public:
  CloudAlarm();
  ~CloudAlarm();
  void trigger(MQTTClient);
  void Stop();
};


#endif /* TRIPWIRE_H_ */

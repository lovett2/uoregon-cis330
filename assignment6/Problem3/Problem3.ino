/*
 * Kathryn Lovett
 * CIS 330, W16
 * Assignment6: Trip Wire
 */
//#include <MQTTClient.h>
#include "TripWire.h"

MQTTClient mqttClient;
 
int photoResistorPin = 0;  // Analog 0
int laserPtrPin = 3;
int lightVal = 0;
int threshold = 975;

void setup() {
  mqttClient.begin("brix.d.cs.uoregon.edu", 8330, OPEN, NULL, NULL, NULL);
  Alarm *a = new Alarm();
  pinMode(laserPtrPin, OUTPUT);  // laser pointer
  pinMode(a->builtinLight, OUTPUT); // built-in light
  pinMode(a->buzzerPin, OUTPUT); // buzzer
  Serial.begin(9600);
}
void loop() {
  //mqttClient.loop();
  digitalWrite(laserPtrPin, HIGH);
  lightVal = analogRead(photoResistorPin);
  Serial.println(lightVal);
  
  if (lightVal < threshold) { 
    BlinkAlarm *a = new BlinkAlarm();
    BuzzerAlarm *b = new BuzzerAlarm();
    CloudAlarm *c = new CloudAlarm();
    a->trigger();
    //b->trigger();
    c->trigger(mqttClient);
    delay(c->alarmDuration);
    a->Stop();
    b->Stop();
    c->Stop();
  }
}


#ifndef DIRECTORY_H_
#define DIRECTORY_H_

/* add a new entry */
void newEntry();

/* removes an entry */
void removeEntry();

/* displays the current directory */
void displayDirectory();

/* free the previously allocated memory */
void freeMem();

#endif /* DIRECTORY_H_ */

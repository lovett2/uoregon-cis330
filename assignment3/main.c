/* Kathryn Lovett
   CIS 330, W16
   Assignment 3
*/
#include <stdio.h>
#include <stdbool.h>
#include "directory.h"

int main() {
	// Declaration of integer to track the choice of the user from
	// the list of options below.
	int choice = 0;

	// Declaration of boolean value to track that the user is still
	// using their address book.
    bool running = true;

    // Do while loop that continues prompting the user for input as 
    // long as the boolean running is set to true. It changes to false
    // when the user selects 4. Close directory. Otherwise, the user can 
    // choose to add an entry, remove an entry, or print the directory.
    do {
        printf("\nPlease choose an option: \n");
        printf("1. Insert a new entry. \n");
        printf("2. Delete an entry. \n");
        printf("3. Display current directory. \n");
        printf("4. Close directory. \n");
        printf("Option: ");
        scanf("%d", &choice);

        switch(choice) {
            case 1 :
                newEntry();
                break;
            case 2 :
                printf("\n");
                removeEntry();
                break;
            case 3 :
                displayDirectory();
                break;
            case 4 :
                printf("\n");
                freeMem();
                running = false;
                break;
        }
    } while(running);

	return 0;
	
}

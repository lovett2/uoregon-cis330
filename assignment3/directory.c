/* Kathryn Lovett
   CIS 330, W16
   Assignment 3
*/
#include <stdio.h>   
#include <string.h> 
#include <stdlib.h>
#include "directory.h"

// Struct declaration that will hold each address entry.
struct addressEntry {
    char name[30];
    char number[30];

    struct addressEntry *next;
};

// Declaration of head and rear pointers.
struct addressEntry *head = NULL;
struct addressEntry *rear = NULL;

// Count will keep track of entries added to directory.
int count = 0;

                                         
void newEntry() {
    // Increase count of directories by 1.
    count++;

    // Declare variables to hold input from user.
    char firstName[30] = "";
    char lastName[30] = "";
    char num[30] = "";

    // Prompt for name and scan.
    printf("Enter a name: ");
    scanf("%s %s", firstName, lastName);

    // Prompt for phone number and scan.
    printf("Enter a phone number: ");
    scanf(" %s", num);

    // Create new struct to hold the new address book entry.
    struct addressEntry *temp = 
        (struct addressEntry *)malloc(sizeof(struct addressEntry));
    
    // Copying the input name into the struct.
    strcpy(temp->name, firstName);
    strcat(temp->name, " ");
    strcat(temp->name, lastName);
    
    // Copying the input phone number into the struct.
    strcpy(temp->number, num);

    // If it is the first entry, make the rear and head
    // equal to the struct temp. Else, make the head.next = temp
    // and head = temp.
    if(rear == NULL && head == NULL) {
        rear = temp;
        head = temp;
    }
    else {
        head->next = temp;
        head = temp;
    }
}

void removeEntry() {
    // Print out the directory so the entry to be deleted can be chosen.
    displayDirectory();
    if(count == 0) {
        printf("No entries exist to delete.\n");
    }
    else {
        // Declaration of integer to hold entry number to be deleted.
        int index = 0;

        // Declaration of struct to use when deleting entry.
        struct addressEntry *current = rear;

        // Prompt for which entry is to be deleted and then scan in their choice.
        printf("Which entry should I delete? ");
        scanf("%d", &index);

        // Depending on if the entry to be deleted is the head, rear, or in the
        // middle of the list, do the following actions in order to remove it 
        // from the list and update the pointers of the surrounding entries.
        // After updating the pointers, free the memory allocated to that entry.
        if(index == 1) {
            rear = current->next;
            free(current);
        }
        else if(index == count) {
            for(int i = 1; i < index - 1; i++) {
                current = current->next;
            }
            head = current;
            free(current->next);
            current->next = NULL;
        }
        else {
            for(int i = 1; i < index - 1; i++) {
                current = current->next;  
            }
            free(current->next);
            current->next = current->next->next;
        }

        // Reduce count of entries by 1.
        count--;
    }
}

void displayDirectory() {
    if(count == 0) {
        printf("\nCurrent directory is empty.\n");
    }
    else {
        printf("\nCurrent directory: \n");

        // Declaration of integer to track the entry number of the entry.
        int entryNum = 1;

        // Declaration of struct to use to loop through entries.
        struct addressEntry *current = rear;

        // Loop through entries and print out each one as long as the next entry
        // isn't equal to NULL.
        do {
            printf("%d. %s, %s\n", entryNum, current->name, current->number);
            current = current->next;
            entryNum++;
        } while(current != NULL);
    }
    
}

void freeMem() {
    // Declaration of two structs to use when looping through entries
    // and freeing the memory allocated to them.
    struct addressEntry *current = rear;
    struct addressEntry *temp;

    // Loop through entries and free them.
    while(current != NULL) {
        temp = current;
        current = current->next;
        free(temp);
    }
}                                                                    
                                                                         

/* Kathryn Lovett
   CIS 330, W16
   Assignment 2
*/

#include <stdio.h>
#define MAX 20

/* Print a triangle of height 5 */
void print5Triangle(){
	printf("Problem 1 (a triangle of height 5):\n");
	
	// Declares 2d array and fills it
	int arr[MAX][MAX] = {
		{0} ,
		{0, 1, 2} ,
		{0, 1, 2, 3, 4} ,
		{0, 1, 2, 3, 4, 5, 6} ,
		{0, 1, 2, 3, 4, 5, 6, 7, 8}
	};
	
	// Declaring variables to track the current row, number of 
	// spaces printed, total number of spaces to print, total number
	// of rows, and the count of how many number to print each row.

	int rows, spaces;
	int number_of_spaces = 10;
	int number_of_rows = 5;
	int count = 3;	
	
	// Loop that prints out a triangle of numbers
	for(rows = 0; rows < number_of_rows; rows++) {
		for(spaces = 0; spaces <= number_of_spaces; spaces++) {
			printf(" ");
		}
		if(rows == 0) {
			printf("%d", arr[rows][0]);
		}
		else {
			for(int i = 0; i < count; i++) {
				printf("%d", arr[rows][i]);
				printf(" ");
			}
			count = count + 2;
		}
		printf("\n");
		number_of_spaces = number_of_spaces - 2;
	}
}	

/* Kathryn Lovett
   CIS 330, W16
   Assignment 2
*/

#include <stdlib.h>
#include <stdio.h>

/* Allocate a triangle of height "height" (a 2-D array of int) */
void allocateNumberTriangle(const int height, int ***triangle) {
	// Allocating memory to 2d array variable triangle
 
	(*triangle) = (int **)malloc(height * sizeof(int *));
	
	for(int i = 0; i < height; ++i) {
		(*triangle)[i] = (int *)malloc(10 * sizeof(int));
	};
}

/* Initialize the 2-D triangle array */
void initializeNumberTriangle(const int height, int **triangle) {
        // Filling 2d array with numbers counting from 0 to 9

	int count = 0;
        for(int i = 0; i < height; i++) {
	        for(int j = 0; j < 10; j++) {
                        triangle[i][j] = count++;
                }
		count = 0;
        }
}

/* Print a formatted triangle */
void printNumberTriangle(const int height, int **triangle) {
	// Prints out 2d array of height 'height' shaped
	// like a triangle.

	int rows, spaces;
        int number_of_spaces = height * 2;
        int count = 3;
	
        for(rows = 0; rows < height; rows++) {
                for(spaces = 0; spaces <= number_of_spaces; spaces++) {
                        printf(" ");
                }
                if(rows == 0) {
                        printf("%d", triangle[rows][0]);
                }
                else {
                        for(int i = 0; i < count; i++) {
                                printf("%d", triangle[rows][i]);
                                printf(" ");
                        }
                        count = count + 2;
                }
                printf("\n");
                number_of_spaces = number_of_spaces - 2;
        }
}

/* Free the memory for the 2-D triangle array */
void deallocateNumberTriangle(const int height, int **triangle) {
        for(int i = 0; i < height; i++) {
               free(triangle[i]);
        };
}

/* Kathryn Lovett
   CIS 330, W16
   Assignment 2
*/

#include <stdio.h>
#include "triangle.h"

int main() {
	// Calling the print5Triangle from problem 1 that is in triangle1.c
	print5Triangle();
	
	// Declaring 2d array triangle and variable to hold height of 
	// triangle as input by user
	int **triangle;
	int size;
	
	// Prompt for height of triangle from until user enters
	// valid input [1-5]
	printf("Problem 2: \n");
	do {
		printf("Please enter the height of the triangle [1-5]: ");
		scanf("%d", &size);
	} while (size < 1 || size > 5);
	
	// Calling functions from problem 2 that allocate 
	// memory for 2d array triangle using height designated
	// by variable size; initialize the numbers in the 2d 
	// triangle; print out the triangle in the correct format;
	// and free the memory allocated to the 2d array.

	allocateNumberTriangle((const int) size, &triangle);
	
	initializeNumberTriangle((const int) size, triangle);

	printNumberTriangle((const int) size, triangle);

	deallocateNumberTriangle((const int) size, triangle);	
	
	return 0;
}


/* File: rps.c
   Kathryn Lovett
   CIS 330 W16
 */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>      
#include <string.h>

char* getUserChoice() {
    /* Prompt the user "Enter rock, paper, or scissors: " and return
       the string they enter */
    
    // Create a character array and allocate space to it
    char *input = NULL;
    input = (char *) malloc (10 * sizeof(char));
    
    // Prompt the user for input and retrieve the input with scanf() 
    printf("Enter rock, paper, or scissors: ");
    scanf("%s", input);
    
    // Make sure they've entered valid input and if so, return it
    if(strcmp(input, "rock") != 0 && strcmp(input, "paper") != 0 
	&& strcmp(input, "scissors") != 0) {
	printf("Invalid user choice, you must enter rock, paper, or scissors.\n");
        exit(1);   
    }
    return input;
}

char* getComputerChoice() {
    srand (time(NULL));
    /* get a pseudo-random integer between 0 and 2 (inclusive) */
    int randChoice = rand() % 3;

    /* If randChoice is 0, return "rock"; if randChoice is 1, 
     return "paper", and if randChoice is 2, return "scissors". */
    
    // Check the randChoice integer and depending on whether it's 0, 1, or 
    // 2, return rock, paper, or scissors respectively
    if(randChoice == 0) {
	return "rock";
    }
    else if(randChoice == 1) {
	return "paper";
    }
    else {
	return "scissors";
    }
}

char* compare(char* choice1, char* choice2) 
{
    /* Implement the logic of the game here. If choice1 and choice2
     are equal, the result should be "This game is a tie."

     Make sure to use strcmp for string comparison.
     */

     // Compare the user input and the computer-generated input
     // Determine whether it is a tie, or which side has won
     if(strcmp(choice1, choice2) == 0) {
           return "This game is a tie.";
     }
     else if(strcmp(choice1, "rock") == 0) {
     	if(strcmp(choice2, "paper") == 0) { 
		return "Paper wins.";
	}
	else {
		return "Rock wins.";
	}
     }
     else if(strcmp(choice1, "paper") == 0) {
     	if(strcmp(choice2, "rock") == 0) {
		return "Paper wins.";
	}
	else {
		return "Scissors win.";
	}
     }
     else {
	if(strcmp(choice2, "rock") == 0) {
		return "Rock wins.";
	}
	else {
		return "Scissors win.";
	}
     }
}

int main(int argc, char** argv) 
{
    char *userChoice = NULL, *computerChoice = NULL, *outcome = NULL;
    
    // Call function getUserChoice to get the user input
    userChoice = getUserChoice();
    // Call function getComputerChoice to get the computer-generated input
    computerChoice = getComputerChoice();
    
    // Compare both of the above strings and assign the winner to outcome
    outcome = compare(userChoice, computerChoice);
    
    // Print out the results of the rps match
    printf("You picked %s.\n", userChoice);
    printf("Computer picked %s.\n", computerChoice);
    printf("%s\n", outcome);
    
    // Free the memory allocated to userChoice
    free(userChoice);
    return 0;
}
